import { expect } from 'chai';
import mock from '../dist/index.js';

describe('next random numeric string test', () => {
    it('should return 10 char string by default.', () => {
        const result = mock.random.nextNumericString();
        expect(result).to.have.lengthOf(10);
    });
    it('should return 20 char string.', () => {
        const size = 20;
        const result = mock.random.nextNumericString(size);
        expect(result).to.have.lengthOf(size);
    });
    it('should return all numbers.', () => {
        const result = mock.random.nextNumericString();
        expect(result).to.match(/^\d+$/g);
    });
    it('should return formatted numbers.', () => {
        const result = mock.random.nextNumericString(12, 'xxx-\\xxxxxxxx-xx');
        expect(result).to.match(/^\d{3}-x\d{7}-\d{2}$/g);
    });
});
