import { expect } from 'chai';
import mock from '../dist/index.js';

describe('next random nouns test', () => {
    it('should return a list of nouns (realistic words).', () => {
        const result = mock.realistic.nextNouns();
        result.every(x => expect(x).to.match(/^\w+(?:[-\s]\w+)?$/g));
    });
    it('should return a random number of nouns (realistic words) within a range.', () => {
        const result = mock.realistic.nextNouns({ min: 1, max: 3 });
        expect(result).to.have.length.within(1, 3);
    });
    it('should return a realistic company name.', () => {
        const result = mock.realistic.nextCompanyName();
        expect(result).to.match(/^\w+(?:[-\s]\w+)*$/g);
    });
});
