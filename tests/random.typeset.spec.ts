import { expect } from 'chai';
import mock from '../dist/index.js';

describe('next random typeset (lorem ipsum...) test', () => {
    it('should return paragraph.', () => {
        const result = mock.realistic.nextTypeSet();
        expect(result).to.have.length.gt(20);
    });
});
