import { expect } from 'chai';
import mock from '../dist/index.js';
import dates from '../dist/lib/utils/date.util';

type choosetype = 'one' | 'two' | 'three';

interface Tester {
    num: number;
    bool: boolean;
    date: Date;
    choose: choosetype;
    guid: string;
    fullname: string;
    firstname: string;
    lastname: string;
    company: string;
    street: string;
    city: string;
    state: string;
    zip: string;
    tz: string;
    phone: string;
    email: string;
    title: string;
    typeset: string;
    version: string;
    luhn: string;
    numstring: string;
    dflt: string;
    string: string;
    locations: string[];
}

describe('object filler test', () => {
    const tmplt = {
        num: 'number:20:60',
        bool: 'boolean',
        date: 'date:today-12M:now-1M',
        choose: 'choose:one,two,three',
        guid: 'guid',
        name: '',
        fullname: 'fullname',
        firstname: 'firstname',
        lastname: 'lastname',
        company: 'company',
        street: 'street',
        city: 'city',
        state: 'state',
        zip: 'zip',
        tz: 'tz',
        phone: 'phone',
        email: 'email',
        title: 'title',
        typeset: 'typeset',
        version: 'ver',
        luhn: 'luhn:14:4xxx-xxxx-xxxx-xxx',
        numstring: 'numstring:3:5:Num-xxxxx',
        dflt: '',
        string: 'string:2:6',
        locations: []
    };

    it('should return a filled object.', () => {
        const result = mock.filledObject<Tester>(tmplt, {
            locations: { min: 1, max: 3 }
        });
        expect(result).to.have.property('num').and.to.be.a('number')
            .and.be.within(20, 60);
        expect(result).to.have.property('bool').and.to.be.a('boolean');
        expect(result).to.have.property('date').and.to.be.a('date')
            .and.be.within(dates.addMonths(dates.today(), -12), dates.addMonths(dates.today(), -1));
        expect(result).to.have.property('guid')
            .and.match(/^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/gi);
        expect(result).to.have.property('name')
            .and.match(/^[A-Za-z]{10}$/gi);
        expect(result).to.have.property('fullname').and.to.be.a('string');
        expect(result).to.have.property('firstname').and.to.be.a('string');
        expect(result).to.have.property('lastname').and.to.be.a('string');
        expect(result).to.have.property('company').and.to.be.a('string');
        expect(result).to.have.property('street').and.to.be.a('string');
        expect(result).to.have.property('city').and.to.be.a('string');
        expect(result).to.have.property('state').and.to.be.a('string');
        expect(result).to.have.property('zip').and.to.be.a('string');
        expect(result).to.have.property('tz').and.to.be.a('string');
        expect(result).to.have.property('phone').and.to.be.a('string');
        expect(result).to.have.property('email').and.to.be.a('string');
        expect(result).to.have.property('title').and.to.be.a('string');
        expect(result).to.have.property('typeset').and.to.be.a('string');
        expect(result).to.have.property('version').and.to.be.a('string');
        expect(result).to.have.property('luhn').and.to.match(/^4\d{3}-\d{4}-\d{4}-\d{4}$/g);
        expect(result).to.have.property('dflt').and.to.be.a('string');
        expect(result).to.have.property('string').and.to.be.a('string')
            .and.to.have.property('length').be.within(2, 6);
        expect(result).to.have.property('locations').and.to.be.an('array')
            .and.to.have.property('length').be.within(1, 3);
    });

    it('should return a collection of filled object.', () => {
        const result = mock.filledList(
            mock.filledObject<Tester>(tmplt, {
                locations: { min: 1, max: 3 }
            }), { min: 8, max: 10 });
        expect(result).to.be.an('array').and.to.have.property('length').be.within(8, 10);
    });
});
