import { expect } from 'chai';
import mock from '../dist/index.js';

describe('next random collection test', () => {
    it('should return between 1..10 items.', () => {
        const result = mock.filledList({ int: -1 });
        expect(result).to.have.length.within(1, 10);
    });
    it('should return between 3..5 items.', () => {
        const result = mock.filledList({ int: -1 }, { min: 3, max: 5 });
        expect(result).to.have.length.within(3, 5);
    });
    it('should return a list of objects filled with random numbers.', () => {
        const result = mock.filledList({ int: -1 });
        result.every(x => expect(x).to.have.property('int').and.be.within(0, 9));
    });
    it('should return a list of objects filled with arrays between 4 and 6 in length.', () => {
        const result = mock.filledList({ ary: [] }, undefined, { ary: { min: 4, max: 6 } });
        result.every(x => expect(x).to.have.property('ary').and.have.length.within(4, 6));
    });
});
