import { expect } from 'chai';
import mock from '../dist/index.js';

describe('next random boolean test', () => {
    it('should return random true or false.', () => {
        const result = Array.from(new Array(20), () => mock.random.nextBool());
        expect(result).to.contain(true).and.to.contain(false);
    });
});
