import { expect } from 'chai';
import mock from '../dist/index.js';
import { USABank, USStateKey } from '../dist/lib/banks/usa.bank';

const stateKeys = Object.keys(USABank.States);
const stateNames = stateKeys.map(x => USABank.States[x as USStateKey]);
const nextFormat = () => mock.realistic.nextItem<'full' | 'abbrev'>(['full', 'abbrev']);
const nextZipFormat = () => mock.realistic.nextItem<'standard' | 'plusfour'>(['standard', 'plusfour']);

describe('next random address test', () => {
    it('should return a random realistic street.', () => {
        const result = mock.realistic.nextStreet(nextFormat());
        expect(result).to.match(/^\d+(?:\s?\w+-?)+$/g);
    });
    it('should return a random realistic city.', () => {
        const result = mock.realistic.nextCity();
        expect(result).to.match(/^(?:\s?\w+-?)+$/g);
    });
    it('should return a random realistic US state.', () => {
        const format = nextFormat();
        const states = format === 'abbrev' ? stateKeys : stateNames;
        const result = mock.realistic.nextUSState(format);
        expect(result).to.be.oneOf(states);
    });
    it('should return a random realistic US postal code.', () => {
        const result = mock.realistic.nextUSPostCode(undefined, nextZipFormat());
        expect(result).to.match(/^\d{5}(?:-\d{4})?$/g);
    });
    it('should return a random realistic US postal code for a state abbreviation.', () => {
        const state = mock.realistic.nextItem(stateKeys);
        const result = mock.realistic.nextUSPostCode(state, nextZipFormat());
        expect(result).to.match(/^\d{5}(?:-\d{4})?$/g);
    });
    it('should return a random realistic US postal code for a state name.', () => {
        const state = mock.realistic.nextItem(stateNames);
        const result = mock.realistic.nextUSPostCode(state, nextZipFormat());
        expect(result).to.match(/^\d{5}(?:-\d{4})?$/g);
    });
});
