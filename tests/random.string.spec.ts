import { expect } from 'chai';
import mock from '../dist/index.js';

describe('next random string test', () => {
    it('should return 10 char string by default.', () => {
        const result = mock.random.nextString();
        expect(result).to.have.lengthOf(10);
    });
    it('should return 20 char string.', () => {
        const size = 20;
        const result = mock.random.nextString(size);
        expect(result).to.have.lengthOf(size);
    });
    it('should return uppercase string by default.', () => {
        const result = mock.random.nextString();
        expect(result).to.match(/^[A-Z]+$/g);
    });
    it('should return lowercase string.', () => {
        const result = mock.random.nextString(undefined, 'lower');
        expect(result).to.match(/^[a-z]+$/g);
    });
    it('should return mixed case string.', () => {
        const result = mock.random.nextString(undefined, 'mixed');
        expect(result).to.match(/^[A-Za-z]+$/g);
    });
    it('should return formatted string.', () => {
        const result = mock.random.nextString(12, undefined, 'xxx-\\xxxxxxxx-xx');
        expect(result).to.match(/^[A-Za-z]{3}-x[A-Za-z]{7}-[A-Za-z]{2}$/g);
    });
});
