import { expect } from 'chai';
import mock from '../dist/index.js';
import dates from '../dist/lib/utils/date.util';

describe('next random date test', () => {
    it('should return random datetime between Jan 1 1950 and now by default.', () => {
        const result = mock.random.nextDate();
        expect(result).to.be.within(new Date(1950, 1, 1), new Date(Date.now()));
    });
    it('should return random datetime between now and tomorrow.', () => {
        const now = dates.now();
        const tomorrow = dates.addDays(dates.now(), 1);
        const result = mock.random.nextDate(now, tomorrow);
        expect(result).to.be.within(now, tomorrow);
    });
});
