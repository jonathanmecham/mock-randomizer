import { expect } from 'chai';
import mock from '../dist/index.js';

describe('next random email test', () => {
    it('should return a random realistic email.', () => {
        const result = mock.realistic.nextEmail();
        expect(result.local).to.match(/^[a-z.]+$/g);
        expect(result.domain).to.match(/^[a-z.-]+$/g);
        expect(result.tld).to.match(/^[a-z]{2,3}$/g);
    });
    it('should return a random realistic email string.', () => {
        const result = mock.realistic.nextEmailString();
        expect(result).to.match(/^[a-z.]+@[a-z.-]+\.[a-z]{2,3}$/g);
    });
});
