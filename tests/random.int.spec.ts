import { expect } from 'chai';
import mock from '../dist/index.js';

describe('next random integer test', () => {
    it('should return 0..9 with default { min: 0, max: 10 }.', () => {
        const result = mock.random.nextInt();
        expect(result).to.be.within(0, 9);
    });
    it('should return 1..9 with { min: 1 }, max defaults to 10.', () => {
        const result = mock.random.nextInt({ min: 1 });
        expect(result).to.be.within(1, 9);
    });
    it('should return 0..8 with { max: 9 }, min defaults to 0.', () => {
        const result = mock.random.nextInt({ max: 9 });
        expect(result).to.be.within(0, 8);
    });
    it('should return 1..8 with { min: 1, max: 9 }.', () => {
        const result = mock.random.nextInt({ min: 1, max: 9 });
        expect(result).to.be.within(1, 8);
    });
    it('should return 1..9 with { min: 1, max: 9 } inclusive.', () => {
        const result = mock.random.nextInt({ min: 1, max: 9 }, true);
        expect(result).to.be.within(1, 9);
    });
    it('should return valid ranges for multiple iterations.', () => {
        const result = Array.from(new Array(50), () => mock.random.nextInt({ min: 200, max: 401 }));
        result.every(x => expect(x).to.be.within(200, 400));
    });
});
