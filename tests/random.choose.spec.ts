import { expect } from 'chai';
import mock from '../dist/index.js';

const ary = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten'];

describe('next random item from collection test', () => {
    it('should return an item from a list.', () => {
        const result = mock.realistic.nextItem(ary);
        expect(result).to.be.oneOf(ary);
    });
    it('should return an item from a list excluding the first.', () => {
        const first = ary[0];
        const result = mock.realistic.nextItem(ary, [first]);
        expect(result).to.not.eq(first);
    });
});
