import { expect } from 'chai';
import mock from '../dist/index.js';
import { USABank, USStateKey } from '../dist/lib/banks/usa.bank';

const stateKeys = Object.keys(USABank.States);
const stateNames = stateKeys.map(x => USABank.States[x as USStateKey]);
const nextFormat = () => mock.realistic.nextItem<'full' | 'name' | 'abbrev'>(['full', 'name', 'abbrev']);

describe('next random timezone test', () => {
    it('should return a random realistic US timezone.', () => {
        const result = mock.realistic.nextUsTimeZone(undefined, nextFormat());
        expect(result).to.match(/^\w+(?:-\w+)?(?:\s\(\w+\))?$/g);
    });
    it('should return a random realistic US timezone for a state abbreviation.', () => {
        const state = mock.realistic.nextItem(stateKeys);
        const result = mock.realistic.nextUsTimeZone(state, nextFormat());
        expect(result).to.match(/^\w+(?:-\w+)?(?:\s\(\w+\))?$/g);
    });
    it('should return a random realistic US timezone for a state name.', () => {
        const state = mock.realistic.nextItem(stateNames);
        const result = mock.realistic.nextUsTimeZone(state, nextFormat());
        expect(result).to.match(/^\w+(?:-\w+)?(?:\s\(\w+\))?$/g);
    });
});
