import { expect } from 'chai';
import mock from '../dist/index.js';
import { AddressBank } from '../dist/lib/banks/address.bank';
import { USABank, USStateKey, USTZKey } from '../dist/lib/banks/usa.bank.js';
import { Gender } from '../dist/lib/name.generator.js';
import dates from '../dist/lib/utils/date.util';
import { Luhn } from '../dist/lib/utils/luhn.checksum';

const stateKeys = Object.keys(USABank.States);
const stateNames = stateKeys.map(x => USABank.States[x as USStateKey]);
const suffixKeys = Object.keys(AddressBank.Suffixes);
const suffixes = suffixKeys.map(x => AddressBank.Suffixes[x as keyof typeof AddressBank.Suffixes]);
const timezoneKeys = Object.keys(USABank.TimeZones);
const timezones = Object.keys(USABank.TimeZones).map(x => USABank.TimeZones[x as USTZKey]);

describe('next random object test', () => {
    it('should return object with random integer between 80 and 100.', () => {
        const result = mock.filledObject({ int: -1 }, { min: 80, max: 101 });
        expect(result).to.have.property('int').and.be.within(80, 100);
    });
    it('should return object with random boolean.', () => {
        const obj = { bool: false };
        const result = mock.filledObject(obj);
        expect(result).to.not.eq(obj);
        expect(result).to.have.property('bool').and.be.a('boolean');
    });
    it('should return object with random date.', () => {
        // month is 0-indexed
        const start = new Date(Date.UTC(2020, 0, 1));
        const end = new Date(Date.UTC(2020, 11, 31, 23, 59, 59, 999));
        const result = mock.filledObject({ date: new Date() }, { min: start, max: end });
        expect(result).to.have.property('date').and.be.within(start, end);
    });
    it('should return object with randomized array having a length between 4 and 8.', () => {
        const result = mock.filledObject({ ary: [] }, { ary: { min: 4, max: 8 } });
        expect(result).to.have.property('ary').and.have.length.within(4, 8);
    });
    it('should return object with random integer between 80 and 100 from DSL.', () => {
        const result = mock.filledObject({ int: 'number:80:100' });
        expect(result).to.have.property('int').and.be.within(80, 100);
    });
    it('should return object with random decimal between 0 and 1 from DSL.', () => {
        const result = mock.filledObject({ bigint: 'bigint' });
        expect(result).to.have.property('bigint').and.be.within(0, 1);
    });
    it('should return object with random boolean from DSL.', () => {
        const result = mock.filledObject({ bool: 'boolean' });
        expect(result).to.have.property('bool').and.be.a('boolean');
    });
    it('should return object with \'false\' boolean from DSL.', () => {
        const result = mock.filledObject({ bool: 'boolean:false' });
        expect(result).to.have.property('bool').and.to.eq(false);
    });
    it('should return object with random date from DSL.', () => {
        const result = mock.filledObject({ date: 'date' });
        expect(result).to.have.property('date').and.be.a('date');
    });
    it('should return object with random date in range from DSL.', () => {
        const start = new Date(Date.UTC(2020, 0, 1));
        const end = new Date(Date.UTC(2020, 11, 31, 23, 59, 59, 999));
        const result = mock.filledObject({ date: `date:${JSON.stringify(start)}:${JSON.stringify(end)}` });
        expect(result).to.have.property('date').and.be.within(start, end);
    });
    it('should return object with random date in range from DSL named dates.', () => {
        const result = mock.filledObject({ date: 'date:today:today+1h' });
        expect(result).to.have.property('date').and.be.within(dates.today(), dates.addHours(dates.today(), 1));
    });
    it('should return object with random string from DSL.', () => {
        const result = mock.filledObject({ str: 'string' });
        expect(result).to.have.property('str').and.have.lengthOf(10);
    });
    it('should return object with random string with a length of 8 from DSL.', () => {
        const result = mock.filledObject({ str: 'string:8' });
        expect(result).to.have.property('str').and.have.lengthOf(8);
    });
    it('should return object with random string with a length between 12 and 15 from DSL.', () => {
        const result = mock.filledObject({ str: 'string:12:15' });
        expect(result).to.have.property('str').and.have.length.within(12, 15);
    });
    it('should return object with random string with a length between 12 and 15 from DSL.', () => {
        const result = mock.filledObject({ str: 'string:dflt:dlft:upper' });
        expect(result).to.have.property('str').and.match(/^[A-Z]+$/g);
    });
    it('should return object with random choice in a list from DSL.', () => {
        const result = mock.filledObject({ choice: 'choose:A,B,C,D,E,F' });
        expect(result).to.have.property('choice').and.match(/^[A-F]$/g);
    });
    it('should return object with random guid from DSL.', () => {
        const result = mock.filledObject({ guid: 'guid' });
        expect(result).to.have.property('guid').and.match(/^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/gi);
    });
    it('should return object with random full name from DSL.', () => {
        const result = mock.filledObject({ name: 'fullname' });
        expect(result).to.have.property('name').and.match(/^\w+\s\w+\s\w+$/gi);
    });
    it('should return object with random Male full name from DSL.', () => {
        const result = mock.filledObject({ name: 'fullname:Male' });
        expect(result).to.have.property('name').and.satisfies((x: string) =>
            mock.names.nameMatchesGender(x, Gender.Male)
        );
    });
    it('should return object with random Female full name from DSL.', () => {
        const result = mock.filledObject({ name: 'fullname:Female' });
        expect(result).to.have.property('name').and.satisfies((x: string) =>
            mock.names.nameMatchesGender(x, Gender.Female)
        );
    });
    it('should return object with random first name from DSL.', () => {
        const result = mock.filledObject({ name: 'firstname' });
        expect(result).to.have.property('name').and.match(/^\w+$/gi);
    });
    it('should return object with random last name from DSL.', () => {
        const result = mock.filledObject({ name: 'lastname' });
        expect(result).to.have.property('name').and.match(/^\w+$/gi);
    });
    it('should return object with random company name from DSL.', () => {
        const result = mock.filledObject({ co: 'company' });
        expect(result).to.have.property('co').and.match(/^\w+(?:[-\s]\w+)*$/gi);
    });
    it('should return object with random street from DSL.', () => {
        const result = mock.filledObject({ street: 'street' });
        expect(result).to.have.property('street').and.match(/^\d+(?:\s?\w+-?)+$/g);
    });
    it('should return object with random "full" format street from DSL.', () => {
        const result = mock.filledObject({ street: 'street:full' });
        expect(result).to.have.property('street').and.satisfies((x: string) =>
            suffixes.includes(x.split(' ').slice(-1)[0].toUpperCase())
        );
    });
    it('should return object with random "abbrev" format street from DSL.', () => {
        const result = mock.filledObject({ street: 'street:abbrev' });
        expect(result).to.have.property('street').and.satisfies((x: string) =>
            suffixKeys.includes(x.split(' ').slice(-1)[0].toUpperCase())
        );
    });
    it('should return object with random city from DSL.', () => {
        const result = mock.filledObject({ city: 'city' });
        expect(result).to.have.property('city').and.match(/^(?:\s?\w+-?)+$/g);
    });
    it('should return object with random US state from DSL.', () => {
        const result = mock.filledObject({ state: 'state' });
        expect(result).to.have.property('state').and.be.oneOf(stateKeys.concat(stateNames));
    });
    it('should return object with random "full" format US state from DSL.', () => {
        const result = mock.filledObject({ state: 'state:full' });
        expect(result).to.have.property('state').and.be.oneOf(stateNames);
    });
    it('should return object with random "abbrev" format US state from DSL.', () => {
        const result = mock.filledObject({ state: 'state:abbrev' });
        expect(result).to.have.property('state').and.be.oneOf(stateKeys);
    });
    it('should return object with random US postal code from DSL.', () => {
        const result = mock.filledObject({ zip: 'zip' });
        expect(result).to.have.property('zip').and.match(/^\d{5}(?:-\d{4})?$/g);
    });
    it('should return object with random "AZ" postal code from DSL.', () => {
        const result = mock.filledObject({ zip: 'zip:AZ' });
        expect(result).to.have.property('zip').and.match(/^8[5-6][0-9]\d{2}(?:-\d{4})?$/g);
    });
    it('should return object with random "standard" format US postal code from DSL.', () => {
        const result = mock.filledObject({ zip: 'zip:state:standard' });
        expect(result).to.have.property('zip').and.match(/^\d{5}$/g);
    });
    it('should return object with random "plusfour" format US postal code from DSL.', () => {
        const result = mock.filledObject({ zip: 'zip:state:plusfour' });
        expect(result).to.have.property('zip').and.match(/^\d{5}-\d{4}$/g);
    });
    it('should return object with random US timezone from DSL.', () => {
        const result = mock.filledObject({ tz: 'tz' });
        expect(result).to.have.property('tz').and.match(/^\w+(?:-\w+)?(?:\s\(\w+\))?$/g);
    });
    it('should return object with random "AZ" timezone from DSL.', () => {
        const result = mock.filledObject({ tz: 'tz:AZ:abbrev' });
        expect(result).to.have.property('tz').and.match(/^M[SD]T$/g);
    });
    it('should return object with random "full" format US timezone from DSL.', () => {
        const result = mock.filledObject({ tz: 'tz:state:full' });
        expect(result).to.have.property('tz').and.match(/^\w+(?:-\w+)?\s\(\w+\)$/g);
    });
    it('should return object with random "abbrev" format US timezone from DSL.', () => {
        const result = mock.filledObject({ tz: 'tz:state:abbrev' });
        expect(result).to.have.property('tz').and.be.oneOf(timezoneKeys);
    });
    it('should return object with random "name only" format US timezone from DSL.', () => {
        const result = mock.filledObject({ tz: 'tz:state:name' });
        expect(result).to.have.property('tz').and.be.oneOf(timezones);
    });
    it('should return object with random US phone from DSL.', () => {
        const result = mock.filledObject({ phone: 'phone' });
        expect(result).to.have.property('phone').and.match(/^\(\d{3}\)\s\d{3}-\d{4}$/g);
    });
    it('should return object with random "Idaho" state phone from DSL.', () => {
        const result = mock.filledObject({ phone: 'phone:Idaho' });
        expect(result).to.have.property('phone').and.match(/^\(208\)\s\d{3}-\d{4}$/g);
    });
    it('should return object with random email from DSL.', () => {
        const result = mock.filledObject({ email: 'email' });
        expect(result).to.have.property('email').and.match(/^[a-z.]+@[a-z.-]+\.[a-z]{2,3}$/g);
    });
    it('should return object with random book title from DSL.', () => {
        const result = mock.filledObject({ title: 'title' });
        expect(result).to.have.property('title').and.match(/^(?:\s?\w+-?)+$/g);
    });
    it('should return object with random typeset (lorem ipsum...) from DSL.', () => {
        const result = mock.filledObject({ typeset: 'typeset' });
        expect(result).to.have.property('typeset').and.to.have.length.gt(20);
    });
    it('should return object with random app version from DSL.', () => {
        const result = mock.filledObject({ version: 'ver' });
        expect(result).to.have.property('version').and.match(/^\d{1,2}\.\d{1,2}(?:\.\d{1,2}(?:\.\d{1,2})?)?$/g);
    });
    it('should return object with random code that passes Luhn checksum validation from DSL.', () => {
        const result = mock.filledObject({ luhn: 'luhn:14:4xxxxxxxxxxxxxx' });
        expect(result).to.have.property('luhn').and.satisfy((x: string) => Luhn.valid(x));
    });
});
