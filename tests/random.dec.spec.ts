import { expect } from 'chai';
import mock from '../dist/index.js';

describe('next random decimal test', () => {
    it('should return 0..1 with default { min: 0, max: 1 }.', () => {
        const result = mock.random.nextDec();
        expect(result).to.be.within(0, 1);
    });
});
