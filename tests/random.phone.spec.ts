import { expect } from 'chai';
import mock from '../dist/index.js';

describe('next random phone test', () => {
    it('should return a random realistic US phone number.', () => {
        const result = mock.realistic.nextUSPhone();
        expect(result.npa).to.match(/^\d{3}$/g);
        expect(result.nxx).to.match(/^\d{3}$/g);
        expect(result.slid).to.match(/^\d{4}$/g);
    });
    it('should return a random realistic US phone number string.', () => {
        const result = mock.realistic.nextUSPhoneString();
        expect(result).to.match(/^\(\d{3}\)\s\d{3}-\d{4}$/g);
    });
});
