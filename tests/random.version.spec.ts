import { expect } from 'chai';
import mock from '../dist/index.js';

describe('next random version test', () => {
    it('should return a realistic app version.', () => {
        const result = mock.realistic.nextVersion(mock.random.nextBool(), mock.random.nextBool());
        expect(result).to.match(/^\d{1,2}\.\d{1,2}(?:\.\d{1,2}(?:\.\d{1,2})?)?$/g);
    });
});
