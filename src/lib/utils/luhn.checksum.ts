export class Luhn {
    public static checksum = (code: string): number => {
        const nums = code.replace(/\D/g, '');
        const parity = nums.length % 2;
        let sum = 0;
        for (let i = nums.length - 1; i > -1; i--) {
            let d = parseInt(nums.charAt(i), 10);
            if (i % 2 === parity) { d *= 2; }
            if (d > 9) { d -= 9; }
            sum += d;
        }
        return sum % 10;
    }

    public static gen = (code: string): string => {
        let chksum = Luhn.checksum(`${code}0`);
        chksum = chksum === 0 ? 0 : 10 - chksum;
        return `${code}${chksum}`;
    }

    public static valid = (code: string): boolean => Luhn.checksum(code) === 0;
}
