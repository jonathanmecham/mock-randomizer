export const toBool = (str: string): boolean => {
    let bool = false;
    const s = (str || '').toLowerCase().trim();
    switch (s) {
        case 'yes': case 'true': bool = true; break;
        default: bool = isNaN(+(s || 'NaN')) ? false : +s > 0; break;
    }
    return bool;
};

export const asBool = (str: string): boolean | undefined => {
    let bool: boolean | undefined;
    const s = (str || '').toLowerCase().trim();
    switch (s) {
        case 'true': case 'yes': bool = true; break;
        case 'false': case 'no': bool = false; break;
        default: bool = isNaN(+(s || 'NaN')) ? undefined : +s > 0; break;
    }
    return bool;
};

export const isBool = (str: string): boolean => {
    const bool = asBool(str);
    return bool === true || bool === false;
};

export const toNum = (str: string, dflt?: number): number => {
    let n = +str.trim();
    if (!n) {
        n = dflt || 0;
    }
    return n;
};
