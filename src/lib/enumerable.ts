export class Enumerable {
    /// generates a sequence of integral numbers within a specified range and returns them as a Promise.
    public static ofRange = (start: number, count: number): Promise<number[]> => {
        return Promise.resolve(Enumerable.range(start, count));
    }

    /// generates a sequence of integral numbers within a specified range.
    public static range = (start: number, count: number): number[] => {
        return Array.from(new Array(count), (val, index) => index + start);
    }
}
