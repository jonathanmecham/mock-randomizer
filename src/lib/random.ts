import { Enumerable } from './enumerable';
import { normalizeRange, NumericRange } from './numeric.range';
import { Realistic } from './realistic';

export class Random {
    /// returns a random interger within the specified range.
    public static nextInt = (range?: NumericRange, inclusive: boolean = false): number => {
        const rng = normalizeRange(range || {});
        return Math.floor(Math.random() * Math.floor(rng.max - rng.min + +inclusive)) + rng.min;
    }

    /// returns a random decimal within the specified range.
    public static nextDec = (range?: NumericRange): number => {
        const rng = normalizeRange(range || {}, { min: 0, max: 1 });
        return Math.random() * Math.floor(rng.max - rng.min) + rng.min;
    }

    /// generates a randomly true or false value.
    public static nextBool = (): boolean => {
        return !!Random.nextInt({ max: 1 }, true);
    }

    /// generates a new string string of numeric characters that is the length of the specified size.
    public static nextNumericString = (size?: number, format?: string) => {
        let value = Enumerable.range(0, size || 10).map(x => Random.nextInt()).join('');
        if (format) {
            let i = 0;
            value = format.replace(/\\?./g, c => {
                return c === 'x' ? value[i++] || c : c.slice(+/^\\/.test(c));
            });
        }
        return value;
    }

    /// generates a new string of random alphabetic characters that is the length of the speicfied size.
    public static nextString = (size: number = 10, cased?: 'upper' | 'lower' | 'mixed', format?: string): string => {
        size = size < 1 ? 10 : size;
        let value = '';
        let firstChar = () => 65; // 65 = A
        switch (cased) {
            case 'lower':
                firstChar = () => 97; // 97 = a
                break;
            case 'mixed':
                firstChar = () => Realistic.nextItem([65, 97]);
                break;
        }
        Enumerable.range(0, size)
            .map(() => String.fromCharCode(firstChar() + Random.nextInt({ max: 26 })))
            .forEach(x => value += x);
        if (format) {
            let i = 0;
            value = format.replace(/\\?./g, c => {
                return c === 'x' ? value[i++] || c : c.slice(+/^\\/.test(c));
            });
        }
        return value;
    }

    /// generates a random date within a specified range.
    public static nextDate = (startDate?: Date, endDate?: Date): Date => {
        const rng = normalizeRange({
            min: startDate && startDate.valueOf(),
            max: endDate && endDate.valueOf()
        }, { min: new Date(Date.UTC(1950, 0, 1)).valueOf(), max: new Date(Date.now()).valueOf() });
        const ts = rng.max - rng.min;
        return new Date(rng.min + Random.nextInt({ max: ts }));
    }
}
