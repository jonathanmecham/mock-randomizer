import { NameBank } from './banks/name.bank';
import { WordBank } from './banks/word.bank';
import { Random } from './random';
import { Realistic } from './realistic';

// tslint:disable: no-bitwise

export enum Gender { Unspecified, Male, Female }
export enum NameComponents {
    First = 1 << 0,
    Middle = 1 << 1,
    Last = 2 << 2,
    FirstLast = First | Last,
    Full = First | Middle | Last
}
export interface Name {
    first?: string;
    middle?: string;
    last?: string;
}

export class NameGenerator {
    static nextGender = (): Gender => {
        return Random.nextInt({ min: 0, max: 2 }, true);
    }

    static nextName = (nameComponent: NameComponents, gender = Gender.Unspecified, ...ignoreNames: string[]): Name => {
        const name: Name = {};
        gender = Gender.Unspecified ? Random.nextInt({ min: 1, max: 2 }, true) : gender;
        if ((nameComponent & NameComponents.Last) === NameComponents.Last) {
            name.last = Realistic.nextItem(NameBank.LastNames, ignoreNames);
        }
        const names = gender === Gender.Female ? NameBank.FemaleNames : NameBank.MaleNames;
        if ((nameComponent & NameComponents.Middle) === NameComponents.Middle) {
            name.middle = Realistic.nextItem(names, ignoreNames);
        }
        if ((nameComponent & NameComponents.First) === NameComponents.First) {
            name.first = Realistic.nextItem(names, ignoreNames);
        }
        return name;
    }

    static nextFirstName = (gender = Gender.Unspecified, ...ignoreNames: string[]): string | undefined => {
        const name = NameGenerator.nextName(NameComponents.First, gender, ...ignoreNames);
        return name.first;
    }

    static nextMiddleName = (gender = Gender.Unspecified, ...ignoreNames: string[]): string | undefined => {
        const name = NameGenerator.nextName(NameComponents.Middle, gender, ...ignoreNames);
        return name.middle;
    }

    static nextLastName = (...ignoreNames: string[]): string | undefined => {
        const name = NameGenerator.nextName(NameComponents.Last, Gender.Unspecified, ...ignoreNames);
        return name.last;
    }

    static nextFullName = (gender = Gender.Unspecified, ...ignoreNames: string[]): string => {
        const name = NameGenerator.nextName(NameComponents.Full, gender, ...ignoreNames);
        return `${name.first} ${name.middle} ${name.last}`;
    }

    static nameMatchesGender = (name: string, gender: Gender): boolean => {
        let names: string[];
        switch (gender) {
            case Gender.Female:
                names = NameBank.FemaleNames;
                break;
            case Gender.Male:
                names = NameBank.MaleNames;
                break;
            default:
            case Gender.Unspecified:
                names = NameBank.FemaleNames.concat(NameBank.MaleNames);
                break;
        }
        const parts = name.split(' ');
        return names.some(x => x === parts[0]);
    }

    static nextTitle = (...ignoreWords: string[]): string => {
        const delimiter = ' ';
        const ignoreSet: Set<string> = new Set<string>();
        for (const word of (ignoreWords || [])) {
            const innerWords = word.split(delimiter);
            for (const innerWord of innerWords) {
                ignoreSet.add(innerWord);
            }
        }

        if (!WordBank.Nouns.some(x => !ignoreSet.has(x))) {
            throw new Error('Ignored word list has excluded all available nouns.');
        }

        let prefix = '';
        if (Random.nextInt({ max: 6 }) === 0) {
            prefix = Realistic.nextItem(WordBank.Prefixes.filter(x => !ignoreSet.has(x)));
        }

        let adverb = '';
        if (Random.nextInt({ max: 3 }) === 0) {
            adverb = Realistic.nextItem(WordBank.Adverbs.filter(x => !ignoreSet.has(x)));
        }

        let adjective = '';
        if (adverb || Random.nextBool()) {
            adjective = Realistic.nextItem(WordBank.Adjectives.filter(x => !ignoreSet.has(x)));
        }

        let noun = Realistic.nextItem(WordBank.Nouns.filter(x => !ignoreSet.has(x) && !ignoreSet.has(x + 's') && !ignoreSet.has(x + 'es')));

        if (prefix === 'A' && /^[AaEeIiOoUu]+/g.test(adverb || adjective || noun)) {
            prefix = prefix + 'n';
        }

        let isPlural = Random.nextBool();
        if (['A', 'An'].some(x => x === prefix)) {
            isPlural = false;
        }
        if (isPlural) {
            let end = 's';
            if (noun.endsWith(end)) {
                end = `e${end}`;
            }
            noun = `${noun}${end}`;
        }

        const value = [prefix, adverb, adjective, noun];
        return value.join('&').replace(/&+/g, ' ').trim();
    }
}

// tslint:enable: no-bitwise
