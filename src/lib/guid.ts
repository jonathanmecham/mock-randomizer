// tslint:disable no-bitwise
// tslint:disable: one-variable-per-declaration

export class Guid {
    static newGuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
            const r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
}

// tslint:enable: one-variable-per-declaration
// tslint:enable no-bitwise
