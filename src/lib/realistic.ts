import { AddressBank } from './banks/address.bank';
import { TypeSetBank } from './banks/typeset.bank';
import { USABank, USStateKey, USTZKey } from './banks/usa.bank';
import { WordBank } from './banks/word.bank';
import { Enumerable } from './enumerable';
import { NameComponents, NameGenerator } from './name.generator';
import { NumericRange } from './numeric.range';
import { Random } from './random';

export class Realistic {
    /// selects a random item from a collection.
    public static nextItem = <T>(from: T[], except?: T[]): T => {
        return (from || []).filter(t => !(except || []).some(x => x === t))[Random.nextInt({ min: 0, max: from.length })];
    }

    /// selects a random set of nouns from our word bank.
    public static nextNouns = (range?: NumericRange): string[] => {
        return Enumerable.range(0, Random.nextInt(range, true)).map(x => Realistic.nextItem(WordBank.Nouns));
    }

    /// selects random type set copy (lorem ipsum...)
    public static nextTypeSet = (): string => {
        return Realistic.nextItem(TypeSetBank.TypeSetCopy);
    }

    /// generates realistic output that passes for a US phone number.
    public static nextUSPhone = (state?: string): { npa: number; nxx: number; slid: string; } => {
        if (Object.values(USABank.States).indexOf(state as string) > -1) {
            const [abbrev] = Object.entries(USABank.States).find(([k, v]) => v === state) || [];
            state = abbrev;
        } else {
            state = Realistic.nextItem(Object.keys(USABank.States));
        }
        let npa: number;
        if (USABank.AreaCodes[state as USStateKey]) {
            npa = Realistic.nextItem(USABank.AreaCodes[state as USStateKey]);
        } else {
            npa = Random.nextInt({ min: 200, max: 999 });
        }
        const nxx = Random.nextInt({ min: 200, max: 999 });
        const slid = Random.nextNumericString(4);
        return { npa, nxx, slid };
    }

    /// generates realistic output that passes for a US phone number string.
    public static nextUSPhoneString = (state?: string) => {
        const phone = Realistic.nextUSPhone(state);
        return `(${phone.npa}) ${phone.nxx}-${phone.slid}`;
    }

    /// generates realistic output that passes for an email address.
    public static nextEmail = (local?: string, domain?: string, tld?: string): { local: string; domain: string; tld: string; } => {
        const name = NameGenerator.nextName(NameComponents.FirstLast);
        return {
            local: local || `${name.last}.${name.first}`.toLowerCase(),
            domain: domain || Realistic.nextCompanyName().replace(/\s/g, '.').toLowerCase(),
            tld: tld || Realistic.nextItem(['com', 'org', 'net', 'int', 'edu', 'gov', 'mil', 'us']) // top level domain
        };
    }

    /// generates a random string that passes for an email address.
    public static nextEmailString = (): string => {
        const email = Realistic.nextEmail();
        return `${email.local}@${email.domain}.${email.tld}`;
    }

    /// generates realistic output that passes for an address.
    public static nextStreet = (format?: 'full' | 'abbrev'): string => {
        const num = Random.nextInt({ max: 9999 });
        const street = Realistic.nextNouns({ min: 1, max: 3 }).join(' ');
        const suffix = Realistic.nextItem(format === 'abbrev' ? Object.keys(AddressBank.Suffixes) : Object.values(AddressBank.Suffixes))
            .replace(/\w\S*/g, txt => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());
        return `${num} ${street} ${suffix}`;
    }

    /// generates realistic output that passes for a city.
    public static nextCity = (): string => {
        const city = Realistic.nextNouns({ min: 2, max: 3 }).join(' ');
        let suffix = Realistic.nextItem(Object.values(AddressBank.Suffixes))
            .replace(/\w\S*/g, txt => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());
        if (!suffix.endsWith('s')) { suffix += 's'; }
        return `${city} ${suffix}`;
    }

    // selects a us state from our states collection.
    public static nextUSState = (format?: 'full' | 'abbrev'): string => {
        const state = Realistic.nextItem(format === 'abbrev' ? Object.keys(USABank.States) : Object.values(USABank.States));
        return state;
    }

    // generates realistic output that passes for a US postal code.
    public static nextUSPostCode = (state?: string, format?: 'standard' | 'plusfour'): string => {
        if (Object.values(USABank.States).indexOf(state as string) > -1) {
            const [abbrev] = Object.entries(USABank.States).find(([k, v]) => v === state) || [];
            state = abbrev;
        }

        let zip: string;
        if (USABank.ZipRules[state as USStateKey]) {
            const rule = Realistic.nextItem(USABank.ZipRules[state as USStateKey]);
            const region = Random.nextInt({ min: +rule.regionMin.join(''), max: +rule.regionMax.join('') }).toString().padStart(2, '0');
            const hood = Random.nextNumericString(2);
            zip = `${rule.zone}${region}${hood}`;
        } else {
            zip = Random.nextNumericString(5);
        }
        return format === 'plusfour' ? `${zip}-${Random.nextNumericString(4)}` : zip;
    }

    /// generates realistic output that passes for a US time zone.
    public static nextUsTimeZone = (state?: string, format?: 'full' | 'name' | 'abbrev'): string => {
        if (Object.values(USABank.States).indexOf(state as string) > -1) {
            const [abbrev] = Object.entries(USABank.States).find(([k, v]) => v === state) || [];
            state = abbrev;
        }

        let key: string;
        if (USABank.StateTimeZones[state as USStateKey]) {
            key = Realistic.nextItem(USABank.StateTimeZones[state as USStateKey]);
        } else {
            key = Realistic.nextItem(Object.keys(USABank.TimeZones));
        }
        const tz = USABank.TimeZones[key as USTZKey];

        switch (format) {
            case 'abbrev':
                return key;
            case 'name':
                return tz;
            default:
            case 'full':
                return `${tz} (${key})`;
        }
    }

    /// generates realistic output that passes as a company name.
    public static nextCompanyName = (): string => {
        return Realistic.nextNouns({ min: 1, max: 2 }).join(' ');
    }

    /// generates a random string that passes for a version number.
    public static nextVersion = (build?: boolean, patch?: boolean): string => {
        const count = 2 + +(build === true) + +(build === true && patch === true);
        const version = Array.from(new Array(count), () => Random.nextInt({ max: 11 }));
        return version.join('.');
    }
}
