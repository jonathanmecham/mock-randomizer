export interface NumericRange { min?: number; max?: number; }
export const normalizeRange = (range: NumericRange, dflt?: NumericRange): { min: number, max: number } => {
    const min = range.min || (dflt && dflt.min) || 0;
    const max = range.max || (dflt && dflt.max) || 10;
    return {
        min: Math.max(0, Math.min(max, min)),
        max: Math.min(Math.max(max, min), max)
    };
};
