import dates from './utils/date.util';

export const dsltypes = [
    'bigint',
    'number',
    'boolean',
    'date',
    'string',
    'numstring',
    'choose',
    'guid',
    'fullname',
    'firstname',
    'lastname',
    'company',
    'street',
    'city',
    'state',
    'zip',
    'tz',
    'phone',
    'email',
    'title',
    'typeset',
    'ver',
    'luhn'
] as const;
export type dsltype = (typeof dsltypes)[number];

export const parseDate = (dsl: string): [string, number] => {
    if (+dsl) { return [`${+dsl}`, dsl.length]; }
    const parts = dsl.split(':');
    const start = +dsl.startsWith(':');
    let date = parts.slice(start, 3 + start).join(':');
    if (!Date.parse(date)) {
        date = parts.slice(start, 2 + start).join(':');
    }
    let len = date.length + start;
    date = `${Date.parse(date)}`;
    if (!+date) {
        date = dsl.split(':')[start];
        len = date.length + start;
    }
    return [date, len];
};

export const dateDsl = (dsl: string): string => {
    if (+dsl) { return dsl; }
    let date: Date = new Date(NaN);
    const type = (/^[^\d\+\-]+/g.exec(dsl) || [])[0];
    switch (type) {
        case 'now':
            date = dates.now();
            break;
        case 'today':
            date = dates.today();
            break;
        case 'min':
            date = dates.min();
            break;
        case 'max':
            date = dates.max();
            break;
    }
    if (!+date) { return dsl; }
    let sign: '+' | '-' | undefined;
    for (const mod of dsl.match(/[\+\-]?\d+[YMDhms\.]/g) || []) {
        sign = (/^[\+\-]/g.test(mod) ? mod[0] : sign) as typeof sign;
        const inc = +`${sign || ''}${(mod.match(/\d+/g) || [])[0]}`;
        if (!inc) { continue; }
        switch ((/[YMDhms\.]$/g.exec(mod) || [])[0]) {
            case 'Y':
                date = dates.addYears(date, inc);
                break;
            case 'M':
                date = dates.addMonths(date, inc);
                break;
            case 'D':
                date = dates.addDays(date, inc);
                break;
            case 'h':
                date = dates.addHours(date, inc);
                break;
            case 'm':
                date = dates.addMinutes(date, inc);
                break;
            case 's':
                date = dates.addSeconds(date, inc);
                break;
            case '.':
                date = dates.addMilSeconds(date, inc);
                break;
        }
    }
    return `${+date || dsl}`;
};

export const parseDSL = (dsl: string): [dsltype, string[]] => {
    const value: [dsltype, string[]] = ['string', []];
    const match = /^(?<type>[^:]+)(?<opts>.+)?$/g.exec(dsl);
    if (match?.groups) {
        if (dsltypes.some(x => x === match?.groups?.type)) {
            value[0] = match.groups.type as dsltype;
        }
        value[1] = (match.groups.opts || '').split(':').map(x => x.trim()).filter(x => x);
        if (value[0] === 'date' && !value[1].every(x => +x)) {
            const dtrng = match.groups.opts.replace(/['"']/g, '');
            const [date1, ndx] = parseDate(dtrng);
            const [date2] = parseDate(dtrng.slice(ndx));
            value[1] = [dateDsl(date1), dateDsl(date2)];
        }
    }
    return value;
};

// tslint:disable-next-line: no-any
export const makeDSLRange = (type: string, opts: { min?: any; max?: any; }): string => {
    let hasmin = false;
    let hasmax = false;
    switch (type) {
        case 'number':
        case 'bigint':
            hasmin = !isNaN(opts.min) || !!opts.min;
            hasmax = hasmin && (!isNaN(opts.max) || !!opts.max);
            break;
        case 'date':
            if (opts.min && isNaN(opts.min)) {
                opts.min = new Date(opts.min);
            }
            opts.min = opts.min && opts.min.valueOf();
            if (opts.max && isNaN(opts.max)) {
                opts.max = new Date(opts.max);
            }
            opts.max = opts.max && opts.max.valueOf();

            hasmin = !!opts.min;
            hasmax = hasmin && !!opts.max;
            break;
    }
    return `${type}${hasmin ? `:${opts.min}` : ''}${hasmax ? `:${opts.max}` : ''}`;
};
