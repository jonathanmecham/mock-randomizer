import { Enumerable } from './enumerable';
import { Guid } from './guid';
import { NameGenerator } from './name.generator';
import { normalizeRange, NumericRange } from './numeric.range';
import { Random } from './random';
import { makeDSLRange, parseDSL } from './random.dsl';
import { Realistic } from './realistic';
import { Luhn } from './utils/luhn.checksum';
import { asBool, isBool, toBool } from './utils/string.util';

// tslint:disable: no-any

export type ndx_sig_of<T> = Record<string, T>;

/// generates a collection of items based on an object template.
export const filledList = <T>(template: T | ndx_sig_of<unknown>, range?: NumericRange, inner?: ndx_sig_of<NumericRange>): T[] => {
    const clonefn = (x: ndx_sig_of<any>): T => {
        const keys = Object.keys(x) || [];
        for (const key of keys.filter(k => typeof x[k] === 'object' && !(x[k] instanceof Date))) {
            if (x[key] instanceof Array) {
                x[key] = filledList(x[key][0] || '', inner && inner[key], inner);
            } else {
                x[key] = clonefn(x[key]);
            }
        }
        return { ...x } as T;
    };
    const rng = normalizeRange(range || {}, { min: 1 });
    return Enumerable.range(0, Random.nextInt(rng, true))
        .map(x => filledObject(clonefn({ ...template } as ndx_sig_of<unknown>)));
};

/// fills an object template with randomized data.
export const filledObject = <T>(template: T | ndx_sig_of<unknown>, opts?: ndx_sig_of<any>): T => {
    opts = opts || {};
    switch (typeof template) {
        case 'bigint':
        case 'number':
            return filledObject(makeDSLRange(typeof template, opts)) as {} as T;
        case 'boolean':
            return filledObject('boolean') as {} as T;
        case 'object':
            if (template instanceof Date) {
                return filledObject(makeDSLRange('date', opts)) as {} as T;
            } else {
                const clone: ndx_sig_of<any> = { ...template } ?? {};
                for (const key of Object.keys(clone)) {
                    if (clone[key] instanceof Array) {
                        const rng: NumericRange = opts[key] && ('min' in opts[key] || 'max' in opts[key]) ? opts[key] : undefined;
                        if (rng || !clone[key][0]) {
                            clone[key] = filledList({ prop: clone[key][0] || '' }, rng, opts).map(item => item.prop);
                        } else {
                            clone[key] = clone[key].map((x: any) => filledObject(x, opts));
                        }
                    } else {
                        clone[key] = filledObject(clone[key], opts);
                    }
                }
                return clone as T;
            }
        case 'string':
            const [stype, options] = parseDSL((template || '').trim());
            if (template.length && !template.startsWith(stype)) {
                return template;
            }

            switch (stype) {
                case 'bigint':
                case 'number':
                    options[0] = options[0] || opts.number?.min;
                    options[1] = options[1] || opts.number?.max;
                    options[2] = options[2] || opts.number?.inclusive;
                    const fn = stype === 'bigint' ? Random.nextDec : Random.nextInt;
                    return fn({
                        min: +options[0] || undefined,
                        max: +options[1] || undefined
                    }, asBool(options[2])) as {} as T;
                case 'boolean':
                    const bool = isBool(options[0]) ? toBool(options[0]) : Random.nextBool();
                    return bool as {} as T;
                case 'date':
                    options[0] = options[0] || opts.date?.sdate;
                    options[1] = options[1] || opts.date?.edate;
                    const sdate = +options[0] ? new Date(+options[0]) : undefined;
                    const edate = +options[1] ? new Date(+options[1]) : undefined;
                    return Random.nextDate(sdate, edate) as {} as T;
                case 'choose':
                    options[0] = options[0] || opts.choose;
                    const from = (options[0] || '').split(',').map(x => x.trim()) ||
                        Enumerable.range(0, 5).map(x => `${x}`);
                    return Realistic.nextItem(from) as {} as T;
                case 'guid':
                    return Guid.newGuid() as {} as T;
                case 'fullname':
                case 'firstname':
                case 'lastname':
                    options[0] = options[0] || opts.gender;
                    options[0] = /^unspecified$/gi.test(options[0]) ? '0' : options[0];
                    options[0] = /^male$/gi.test(options[0]) ? '1' : options[0];
                    options[0] = /^female$/gi.test(options[0]) ? '2' : options[0];
                    const gender = +options[0] || NameGenerator.nextGender();
                    switch (stype) {
                        default:
                        case 'fullname':
                            return NameGenerator.nextFullName(gender) as {} as T;
                        case 'firstname':
                            return NameGenerator.nextFirstName(gender) as {} as T;
                        case 'lastname':
                            return NameGenerator.nextLastName() as {} as T;
                    }
                case 'company':
                    return Realistic.nextCompanyName() as {} as T;
                case 'street':
                    options[0] = options[0] || opts.street?.format;
                    const streettypes = ['full', 'abbrev', undefined] as const;
                    type streettype = (typeof streettypes)[number];
                    let streetfmt = Realistic.nextItem(Array.from(streettypes));
                    if (streettypes.some(x => options[0] && x === options[0])) {
                        streetfmt = options[0] as streettype;
                    }
                    return Realistic.nextStreet(streetfmt) as {} as T;
                case 'city':
                    return Realistic.nextCity() as {} as T;
                case 'state':
                    options[0] = options[0] || opts.state?.format;
                    const statetypes = ['full', 'abbrev', undefined] as const;
                    type statetype = (typeof statetypes)[number];
                    let statefmt = Realistic.nextItem(Array.from(statetypes));
                    if (statetypes.some(x => options[0] && x === options[0])) {
                        statefmt = options[0] as statetype;
                    }
                    return Realistic.nextUSState(statefmt) as {} as T;
                case 'zip':
                    options[0] = options[0] || opts.zip?.state;
                    options[1] = options[1] || opts.zip?.format;
                    const zipstate = options[0] === 'state' ? undefined : options[0];
                    const ziptypes = ['standard', 'plusfour', undefined] as const;
                    type ziptype = (typeof ziptypes)[number];
                    let zipfmt = Realistic.nextItem(Array.from(ziptypes));
                    if (ziptypes.some(x => options[1] && x === options[1])) {
                        zipfmt = options[1] as ziptype;
                    }
                    return Realistic.nextUSPostCode(zipstate, zipfmt) as {} as T;
                case 'tz':
                    options[0] = options[0] || opts.tz?.state;
                    options[1] = options[1] || opts.tz?.format;
                    const tzstate = options[0] === 'state' ? undefined : options[0];
                    const tztypes = ['full', 'abbrev', 'name', undefined] as const;
                    type tztype = (typeof tztypes)[number];
                    let tzfmt = Realistic.nextItem(Array.from(tztypes));
                    if (tztypes.some(x => options[1] && x === options[1])) {
                        tzfmt = options[1] as tztype;
                    }
                    return Realistic.nextUsTimeZone(tzstate, tzfmt) as {} as T;
                case 'phone':
                    options[0] = options[0] || opts.phone?.state;
                    return Realistic.nextUSPhoneString(options[0]) as {} as T;
                case 'email':
                    return Realistic.nextEmailString() as {} as T;
                case 'title':
                    return NameGenerator.nextTitle() as {} as T;
                case 'typeset':
                    return Realistic.nextTypeSet() as {} as T;
                case 'ver':
                    return Realistic.nextVersion(Random.nextBool(), Random.nextBool()) as {} as T;
                case 'luhn':
                    options[0] = options[0] || opts.luhn?.size;
                    options[1] = options[1] || opts.luhn?.format;
                    const code = Random.nextNumericString(+options[0] || undefined, options[1]);
                    return Luhn.gen(code) as {} as T;
                case 'numstring':
                    options[0] = options[0] || opts.numstring?.size;
                    let numlen = +options[0] || undefined;
                    if (+options[1] || opts.numstring?.range) {
                        options[0] = options[0] || opts.numstring?.range?.min;
                        options[1] = options[1] || opts.numstring?.range?.max;
                        numlen = Random.nextInt({ min: +options[0], max: +options[1] }, true);
                    }
                    options[2] = options[2] || opts.numstring?.format;
                    return Random.nextNumericString(numlen, options[2]) as {} as T;
                default:
                case 'string':
                    options[0] = options[0] || opts.string?.size;
                    let strlen = +options[0] || undefined;
                    if (+options[1] || opts.string?.range) {
                        options[0] = options[0] || opts.string?.range?.min;
                        options[1] = options[1] || opts.string?.range?.max;
                        strlen = Random.nextInt({ min: +options[0], max: +options[1] }, true);
                    }
                    options[2] = options[2] || opts.string?.cased;
                    const casedtypes = ['upper', 'lower', 'mixed', undefined] as const;
                    type casedtype = (typeof casedtypes)[number];
                    let cased = Realistic.nextItem(Array.from(casedtypes));
                    if (casedtypes.some(x => options[2] && x === options[2])) {
                        cased = options[2] as casedtype;
                    }
                    options[3] = options[3] || opts.string?.format;
                    return Random.nextString(strlen, cased, options[3]) as {} as T;
            }
        default:
        case 'symbol':
        case 'function':
        case 'undefined':
            return undefined as any as T;
    }
};

// tslint:enable: no-any
