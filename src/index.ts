import { filledList, filledObject } from './lib/filler';
import { Guid as guid } from './lib/guid';
import { NameGenerator as names } from './lib/name.generator';
import { Random as random } from './lib/random';
import { Realistic as realistic } from './lib/realistic';

export default { filledList, filledObject, guid, names, random, realistic };
