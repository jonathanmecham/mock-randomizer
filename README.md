Mock Randomizer
===============

fills a structure with randomized data to mock a service or other model provider. number of items in a collection of objects can also be random.

## Installation

  `npm install @joxnathan/mock-randomizer -D`

## Usage
    import mock from '@joxnathan/mock-randomizer';

    this.history.GetChangeHistory = () => Promise.resolve(
      mock.filledList({
        Id: 'guid',
        TimeStamp: 'date:now:now',
        User: { Id: 'number:100:501', Name: 'string:8', Email: 'email' },
        Data: [{
          Id: 'number:1000:2001',
          ChangeType: 'choose:Insert,Update,Delete',
          TableName: 'choose:Claimant,Processor,Advisor,Supervisor,Client,Vendor',
          RecordId: 'number:10:41',
          FieldName: 'choose:Id,Name,Address,City,State,Zip',
          PrevValue: '{}',
          CurrValue: '{}'
        }]
      }, { min: 11, max: 75 }, { Data: { max: 4 } }).map(entry => {
        const getData = () => JSON.stringify(mock.filledObject({
          Id: 'guid',
          version: 'ver',
          Name: '',
          Value: '',
          FullName: 'fullname',
          FirstName: 'firstname',
          LastName: 'lastname',
          Company: 'company',
          Street: 'street',
          City: 'city',
          State: 'state',
          Zip: 'zip',
          Tz: 'tz',
          Phone: 'phone',
          Email: 'email',
          Book: 'title',
          TypeSet: 'typeset',
          Locations: [],
          HasApproval: 'boolean',
          CreatedOn: 'date:today-12M:now-1M',
          CreatedBy: '',
          ModifiedOn: 'date:today-7d:now',
          ModifiedBy: ''
        }, { Locations: { min: 1, max: 3 } }));
        for (const data of entry.Data || []) {
          const vals = {
            prev: ['Delete', 'Update'].includes(data.ChangeType) ? getData() : undefined,
            curr: ['Insert', 'Update'].includes(data.ChangeType) ? getData() : undefined
          };
          data.PrevValue = vals.prev;
          data.CurrValue = vals.curr;
        }
        return entry;
      })
    );

## RunKit Usage
    const mock = require("@joxnathan/mock-randomizer").default;
    const getChangeHistory = () =>
      mock.filledList({
        Id: 'guid',
        TimeStamp: 'date:now:now',
        User: { Id: 'number:100:501', Name: 'string:8', Email: 'email' },
        Data: [{
          Id: 'number:1000:2001',
          ChangeType: 'choose:Insert,Update,Delete',
          TableName: 'choose:Claimant,Processor,Advisor,Supervisor,Client,Vendor',
          RecordId: 'number:10:41',
          FieldName: 'choose:Id,Name,Address,City,State,Zip',
          PrevValue: '{}',
          CurrValue: '{}'
        }]
      }, { min: 11, max: 75 }, { Data: { max: 4 } }).map(entry => {
        const getData = () => JSON.stringify(mock.filledObject({
          Id: 'guid',
          version: 'ver',
          Name: '',
          Value: 'string:4:6:upper',
          FullName: 'fullname',
          FirstName: 'firstname',
          LastName: 'lastname',
          Company: 'company',
          Street: 'street',
          City: 'city',
          State: 'state',
          Zip: 'zip',
          Tz: 'tz',
          Phone: 'phone',
          Email: 'email',
          Book: 'title',
          TypeSet: 'typeset',
          Locations: [],
          HasApproval: 'boolean',
          CreatedOn: 'date:today-12M:now-1M',
          CreatedBy: '',
          ModifiedOn: 'date:today-7d:now',
          ModifiedBy: ''
        }, { Locations: { min: 1, max: 3 } }));
        for (const data of entry.Data || []) {
          const vals = {
            prev: ['Delete', 'Update'].includes(data.ChangeType) ? getData() : undefined,
            curr: ['Insert', 'Update'].includes(data.ChangeType) ? getData() : undefined
          };
          data.PrevValue = vals.prev;
          data.CurrValue = vals.curr;
        }
        return entry;
      });
    const hist = getChangeHistory();
    console.log({ hist });

## Potential Upcoming Features
    1. DSL 'email:[props]' - add option to specify that the value relies on another.
    2. DSL 'string:2 number:1:4 number:1:10 number boolean' - string the dsl commands together in a single property. (not really too sure about this one)

## Contributing

In lieu of a formal style guide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code.